<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_sections', function (Blueprint $table) {
            $table->bigInteger('id')->autoIncrement();
            $table->string('name')->unique()->nullable(false);
            $table->bigInteger('module_id')->nullable(true);
            $table->foreign('module_id')->references('id')->on('access_modules')->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_sections');
    }
}
