<?php

namespace Database\Factories;

use App\Models\AccessModules;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccessModulesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AccessModules::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word
        ];
    }
}
