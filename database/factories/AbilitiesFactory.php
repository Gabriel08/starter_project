<?php

namespace Database\Factories;

use App\Models\Abilities;
use Illuminate\Database\Eloquent\Factories\Factory;

class AbilitiesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Abilities::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => 'user:create',
        ];
    }
}
