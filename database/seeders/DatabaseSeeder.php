<?php

namespace Database\Seeders;

use App\Models\Abilities;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::factory()->count(3)->state( new Sequence(
            [
                'roles' => 'admin-level-3',
                'email' => 'admin3@invt.com'
            ],
            [
                'roles' => 'admin-level-2',
                'email' => 'admin2@invt.com'
            ],
            [
                'roles' => 'admin-level-1',
                'email' => 'admin1@invt.com'
            ],
        ))->create();

        Abilities::factory()->count(23)->state(new Sequence(
            ['name'=>'user:update', 'min_level'=>0],
            ['name'=>'user:create', 'min_level'=>0],
            ['name'=>'user:show', 'min_level'=>1],
            ['name'=>'user:disable', 'min_level'=>1],
            ['name'=>'user:group:add', 'min_level'=>1],
            ['name'=>'user:group:remove', 'min_level'=>1],
            ['name'=>'user:password:reset', 'min_level'=>0],
            ['name'=>'group:update', 'min_level'=>1],
            ['name'=>'group:create', 'min_level'=>1],
            ['name'=>'group:show', 'min_level'=>1],
            ['name'=>'group:delete', 'min_level'=>2],
            ['name'=>'modules:show', 'min_level'=>2],
            ['name'=>'modules:update', 'min_level'=>2],
            ['name'=>'modules:create', 'min_level'=>2],
            ['name'=>'modules:delete', 'min_level'=>3],
            ['name'=>'sections:update', 'min_level'=>2],
            ['name'=>'sections:show', 'min_level'=>2],
            ['name'=>'sections:create', 'min_level'=>2],
            ['name'=>'sections:delete', 'min_level'=>3],
            ['name'=>'menu:update', 'min_level'=>2],
            ['name'=>'menu:create', 'min_level'=>2],
            ['name'=>'menu:show', 'min_level'=>2],
            ['name'=>'menu:delete', 'min_level'=>3]
        ))->create();
    }
}
