<?php

namespace Tests\Feature;

use App\Models\AccessSections;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class SectionDeleteTest extends TestCase
{

    use RefreshDatabase;

    /**
    * test for invalid id supplied by user
    *
    * @return void
    *
    * @test
    */
   public function remove_section_with_invalid_id_format()
   {
       $data = ['id'=>0];

       Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'admin-level-3',
           ]),['section:delete']
       );

       $response = $this->delete('/api/groups/access/sections/remove',$data, ['accept'=>'application/json']);
       $response->assertStatus(400)->assertJsonStructure(['id']);

   }

   /**
    * test if user has the right to delete a section
    *
    * @return void
    *
    * @test
    */
   public function user_access_cannot_delete_section()
   {
       $module = AccessSections::factory()->create();
        $data = ['id'=>$module->id];

     $user = Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'user',
           ]),['user:create']
       );

       $response = $this->delete('/api/groups/access/sections/remove',$data, ['accept'=>'application/json']);
       $response->assertStatus(401)->assertSeeText("User $user->email does not have access");
   }

   /**
    * test if section is found
    *
    * @return void
    *
    * @test
    */
   public function section_not_found()
   {

        $data = ['id'=>45453];

       Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'admin-level-3',
           ]),['section:delete']
       );

       $response = $this->delete('/api/groups/access/sections/remove',$data, ['accept'=>'application/json']);
       $response->assertStatus(404)->assertSeeText("Section with id of {$data['id']} not found");
   }

    /**
    * test for valid section name
    *
    * @return void
    *
    * @test
    */
   public function delete_section_with_valid_credentials()
   {

        $section = AccessSections::factory()->create();
        $data = ['id'=>$section->id];

        $user = Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['section:delete']
        );

        $response = $this->delete('/api/groups/access/sections/remove',$data, ['accept'=>'application/json']);
       $response->assertStatus(200)->assertSeeText("$section->name Section was deleted successfully");

   }

}
