<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class PasswordResetTest extends TestCase
{

    use RefreshDatabase;

    /**
     * tests if the user password has been reset successfully
     *
     * @test
     */

    public function password_reset(){

        $data = ['current_password'=>'@password123F', 'new_password'=> 'password12D@'];

        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'user',
               'password' => Hash::make($data['current_password'] )
            ]),['user:update','user:create','user:show']
        );

         $response = $this->patch('api/password/reset', $data);
         $response->assertStatus(200);
         $response->assertSeeText('password reset successful');

     }

     /**
     * tests if the user password has been reset with same passwords value
     *
     * @test
     */
    public function password_reset_with_same_values(){

        $data = ['current_password'=>'@password123F', 'new_password'=> '@password123F'];

        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'user',
               'password' => $data['current_password']
            ]),['user:update','user:create','user:show']
        );

         $response = $this->patch('api/password/reset', $data);
         $response->assertStatus(400);
         $response->assertSeeText('Current and new password cannot be the same');
     }

       /**
     * tests if the user password has been reset with incurrent password value
     *
     * @test
     */
    public function password_reset_with_incorrect_current_password(){

        $data = ['current_password'=>'@password123F', 'new_password'=> '@password123DF'];

        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'user',
            ]),['user:update','user:create','user:show']
        );

         $response = $this->patch('api/password/reset', $data);
         $response->assertStatus(500);
         $response->assertSeeText('incorrect password');
     }
}
