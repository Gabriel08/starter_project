<?php

namespace Tests\Feature;

use App\Models\AccessModules;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class SectionCreateTest extends TestCase
{
    use RefreshDatabase;

    /**
     * test for invalid name supplied by user e.g name less than the  min value required.
     *
     * @return void
     *
     * @test
     */
    public function create_section_with_invalid_name_format()
    {
        $access_module = AccessModules::factory()->create();
        $data = ['module_id'=> $access_module->id, 'name'=>'A#'];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['section:create']
        );

        $response = $this->post('/api/groups/access/sections/create',$data, ['accept'=>'application/json']);
        $response->assertStatus(400)->assertJsonStructure(['name']);
    }

     /**
     * test for invalid id supplied by user
     *
     * @return void
     *
     * @test
     */
    public function create_section_with_invalid_group_id_format()
    {
        AccessModules::factory()->create();
        $data = ['module_id'=>0, 'name'=>'Regulations'];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['section:create']
        );

        $response = $this->post('/api/groups/access/sections/create',$data, ['accept'=>'application/json']);

        $response->assertStatus(400)->assertJsonStructure(['module_id']);
    }

    /**
     * test if user has the right to create a new section
     *
     * @return void
     *
     * @test
     */
    public function user_cannot_create_section()
    {
        $module = AccessModules::factory()->create();
        $data = ['module_id'=>$module->id, 'name'=>'Regulations'];

        $user = Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'user',
            ]),['user:create']
        );

        $response = $this->post('/api/groups/access/sections/create',$data, ['accept'=>'application/json']);
        $response->assertStatus(401)->assertSeeText("User $user->email does not have access");
    }

     /**
     * test for valid section name
     *
     * @return void
     *
     * @test
     */
    public function create_section_with_valid_credentials()
    {

        $access_module = AccessModules::factory()->create();
        $data = ['module_id'=>$access_module->id, 'name'=>'Regulations'];
        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['section:create']
        );

        $response = $this->post('/api/groups/access/sections/create',$data, ['accept'=>'application/json']);

        $response->assertStatus(201)->assertJsonStructure([
            'name','module_id','id', 'updated_at', 'created_at'
        ]);
    }
}
