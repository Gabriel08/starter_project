<?php

namespace Tests\Feature;

use App\Models\Abilities;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;


    /**
     * validates the the user credentials are valid
     * @return void
     *
     * @test
     */
    public function user_login_with_valid_credentials()
    {
        Abilities::factory()->count(23)->state(new Sequence(
            ['name'=>'user:update', 'min_level'=>0],
            ['name'=>'user:create', 'min_level'=>0],
            ['name'=>'user:show', 'min_level'=>1],
            ['name'=>'user:disable', 'min_level'=>1],
            ['name'=>'user:group:add', 'min_level'=>1],
            ['name'=>'user:group:remove', 'min_level'=>1],
            ['name'=>'user:password:reset', 'min_level'=>0],
            ['name'=>'group:update', 'min_level'=>1],
            ['name'=>'group:create', 'min_level'=>1],
            ['name'=>'group:show', 'min_level'=>1],
            ['name'=>'group:delete', 'min_level'=>2],
            ['name'=>'modules:show', 'min_level'=>2],
            ['name'=>'modules:update', 'min_level'=>2],
            ['name'=>'modules:create', 'min_level'=>2],
            ['name'=>'modules:delete', 'min_level'=>3],
            ['name'=>'sections:update', 'min_level'=>2],
            ['name'=>'sections:show', 'min_level'=>2],
            ['name'=>'sections:create', 'min_level'=>2],
            ['name'=>'sections:delete', 'min_level'=>3],
            ['name'=>'menu:update', 'min_level'=>2],
            ['name'=>'menu:create', 'min_level'=>2],
            ['name'=>'menu:show', 'min_level'=>2],
            ['name'=>'menu:delete', 'min_level'=>3]
        ))->create();


        $input = [
            'email' => 'gabby@gmail.com',
            'password' => '@password12D'
        ];

        $user =  User::factory(1)->create(
            [
                'email' => $input['email']
            ]

        );

        $response = $this->post('api/login', $input,['Accept'=>'Application/json']);
        $response->assertJsonStructure([
            'token',
            'user'
        ]);

        $response->assertStatus(200);
    }

     /**
     * validates the the user credentials are valid
     * @return void
     *
     * @test
     */
    public function user_login_with_invalid_email()
    {

        $input = [
            'email' => 'gabby gmail.com',
            'password' => '@password12D'
        ];

        $user =  User::factory(1)->create(
            [
                'email' => $input['email']
            ]

        );

        $response = $this->post('api/login', $input,['Accept'=>'Application/json']);
        $response->assertJsonStructure([
            'email',
        ]);

        $response->assertStatus(400);
    }

     /**
     * validates the the user password is invalid
     * @return void
     *
     * @test
     */
    public function user_login_with_invalid_password()
    {

        $input = [
            'email' => 'gabby@gmail.com',
            'password' => '@password1'
        ];

        $user =  User::factory(1)->create(
            [
                'email' => $input['email']
            ]

        );

        $response = $this->post('api/login', $input,['Accept'=>'Application/json']);
        $response->assertJsonStructure([
            'password'
        ]);

        $response->assertStatus(400);
    }
    /**
     * tests that a logged in user is logged out
     * @test
     */
    public function logout(){

        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );
            $response = $this->get('api/logout');
            $response->assertSeeText('user logged out');
            $response->assertStatus(200);
    }

}
