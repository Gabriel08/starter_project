<?php

namespace Tests\Feature;

use App\Models\AccessModules;
use App\Models\Group;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ModulesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * test for invalid name supplied by user e.g name less than the  min value required.
     *
     * @return void
     *
     * @test
     */
    public function create_module_with_invalid_name_format()
    {
        $group = Group::factory()->create();
        $data = ['group_id'=> $group->id, 'name'=>'A#'];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['module:create']
        );

        $response = $this->post('/api/groups/access/modules/create',$data, ['accept'=>'application/json']);
        $response->assertStatus(400)->assertJsonStructure(['name']);
    }

     /**
     * test for invalid name supplied by user e.g name less than the  min value required.
     *
     * @return void
     *
     * @test
     */
    public function create_module_with_invalid_group_id_format()
    {
        Group::factory()->create();
        $data = ['group_id'=>0, 'name'=>'Regulations'];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['module:create']
        );

        $response = $this->post('/api/groups/access/modules/create',$data, ['accept'=>'application/json']);
        $response->assertStatus(400)->assertJsonStructure(['group_id']);
    }

    /**
     * test if user has the right to create a new module
     *
     * @return void
     *
     * @test
     */
    public function user_cannot_create_module()
    {
        $group = Group::factory()->create();
        $data = ['group_id'=>$group->id, 'name'=>'Regulations'];

        $user = Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'user',
            ]),['group:create']
        );

        $response = $this->post('/api/groups/access/modules/create',$data, ['accept'=>'application/json']);
        $response->assertStatus(401)->assertSeeText("User $user->email does not have access");
    }

     /**
     * test for valid module name
     *
     * @return void
     *
     * @test
     */
    public function create_module_with_valid_credentials()
    {
        $group = Group::factory()->create();
        $data = ['group_id'=>$group->id, 'name'=>'Regulations'];
        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['module:create']
        );

        $response = $this->post('/api/groups/access/modules/create',$data, ['accept'=>'application/json']);
        $response->assertStatus(201)->assertJsonStructure([
            'name','group_id', 'updated_at', 'created_at','id'
        ]);
    }
}
