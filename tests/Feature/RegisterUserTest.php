<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegisterUserTest extends TestCase
{
    use RefreshDatabase;



    /**
     * when user submits all valid inputs
     * @test
     */
    public function validate_registration_credentials(){
        $user_details = [
            'name' => 'Omokpo Gabriel',
            'email' => 'gabby@gmail.com',
            'password' => '@password12D'
        ];
            $response = $this->post('/api/users/create', $user_details);
            $response->assertCreated()->assertJsonStructure([
                'user'
            ]);
    }

    /**
     * when user submits all wrong email format
     * @test
     */
    public function validate_email_format(){
        $user_details = [
            'name' => 'Omokpo Gabriel',
            'email' => 'gabby>gmail.com',
            'password' => '@password12D'
        ];
            $response = $this->post('/api/users/create', $user_details);
            $response->assertStatus(400)->assertJsonStructure([
                'email'
            ]);
    }

      /**
     * when user submits all wrong name format
     * @test
     */
    public function validate_name_format(){
        $user_details = [
            'name' => 'Omokpo #$Gabriel',
            'email' => 'gabby@gmail.com',
            'password' => '@password12D'
        ];
            $response = $this->post('/api/users/create', $user_details);
            $response->assertStatus(400)->assertJsonStructure([
                'name'
            ]);
    }


      /**
     * when user submits all wrong password format or a conpromised
     * @test
     */
    public function validate_password_format(){
        $user_details = [
            'name' => 'Omokpo Gabriel',
            'email' => 'gabby@gmail.com',
            'password' => '@password'
        ];
            $response = $this->post('/api/users/create', $user_details);
            $response->assertStatus(400)->assertJsonStructure([
                'password'
            ]);
    }
}
