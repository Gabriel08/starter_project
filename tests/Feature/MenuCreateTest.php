<?php

namespace Tests\Feature;

use App\Models\AccessSections;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class MenuCreateTest extends TestCase
{

    use RefreshDatabase;

    /**
     * test for invalid name supplied by user e.g name less than the  min value required.
     *
     * @return void
     *
     * @test
     */
    public function create_menu_with_invalid_name_format()
    {
        $access_section = AccessSections::factory()->create();
        $data = ['section_id'=> $access_section->id, 'name'=>'A#'];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['menu:create']
        );

        $response = $this->post('/api/groups/access/menus/create',$data, ['accept'=>'application/json']);
        $response->assertStatus(400)->assertJsonStructure(['name']);
    }

     /**
     * test for invalid id supplied by user
     *
     * @return void
     *
     * @test
     */
    public function create_menu_with_invalid_group_id_format()
    {
        AccessSections::factory()->create();
        $data = ['section_id'=>0, 'name'=>'Financing'];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['menu:create']
        );

        $response = $this->post('/api/groups/access/menus/create',$data, ['accept'=>'application/json']);

        $response->assertStatus(400)->assertJsonStructure(['section_id']);
    }

    /**
     * test if user has the right to create a new menu
     *
     * @return void
     *
     * @test
     */
    public function user_cannot_create_section()
    {
        $section = AccessSections::factory()->create();
        $data = ['section_id'=>$section->id, 'name'=>'Regulati'];

        $user = Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'user',
            ]),['section:create']
        );

        $response = $this->post('/api/groups/access/menus/create',$data, ['accept'=>'application/json']);
        $response->assertStatus(401)->assertSeeText("User $user->email does not have access");
    }

     /**
     * test for valid menu name
     *
     * @return void
     *
     * @test
     */
    public function create_section_with_valid_credentials()
    {

        $access_section = AccessSections::factory()->create();
        $data = ['section_id'=>$access_section->id, 'name'=>'Regulat'];
        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['menu:create']
        );

        $response = $this->post('/api/groups/access/menus/create',$data, ['accept'=>'application/json']);
        $response->assertStatus(201)->assertJsonStructure([
            'name','section_id','id', 'updated_at', 'created_at'
        ]);
    }
}
