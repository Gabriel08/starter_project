<?php

namespace Tests\Feature;

use App\Models\AccessMenu;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class MenuUpdateTest extends TestCase
{
    use RefreshDatabase;
    /**
    * test for invalid name supplied by user
    *
    * @return void
    *
    * @test
    */
   public function update_section_with_invalid_name_format()
   {
       $data = ['name'=>'Asd#','id'=>1];

       Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'admin-level-3',
           ]),['menu:update']
       );

       $response = $this->patch('/api/groups/access/menus/update',$data, ['accept'=>'application/json']);

       $response->assertStatus(400)->assertJsonStructure([
           'name'
       ]);

   }

    /**
    * test for invalid id supplied by user
    *
    * @return void
    *
    * @test
    */
   public function update_section_with_invalid_id_format()
   {
       $data = ['name'=>'Asd Hello','id'=>0];

       Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'admin-level-3',
           ]),['menu:update']
       );

       $response = $this->patch('/api/groups/access/menus/update',$data, ['accept'=>'application/json']);

       $response->assertStatus(400)->assertJsonStructure(['id']);

   }

   /**
    * test if user has the right to update a menu
    *
    * @return void
    *
    * @test
    */
   public function user_access_cannot_update_section()
   {
       $menu = AccessMenu::factory()->create();
        $data = ['name'=>'Asd Hello','id'=>$menu->id];

      $user =  Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'user',
           ]),['user:show']
       );

       $response = $this->patch('/api/groups/access/menus/update',$data, ['accept'=>'application/json']);
       $response->assertStatus(401)->assertSeeText("User $user->email does not have access");
   }

   /**
    * test if user has the right to update a section
    *
    * @return void
    *
    * @test
    */
   public function section_not_found()
   {

        $data = ['name'=>'Asd Hello','id'=>3];

       Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'admin-level-3',
           ]),['menu:update']
       );

       $response = $this->patch('/api/groups/access/menus/update',$data, ['accept'=>'application/json']);
       $response->assertStatus(404)->assertSeeText("Menu not found");
   }

    /**
    * test for valid module name
    *
    * @return void
    *
    * @test
    */
   public function update_section_with_valid_credentials()
   {

       Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'admin-level-3',
           ]),['menu:update']
       );

       $section = AccessMenu::factory()->create();
       $data = ['name'=>'Ternational','id' =>$section->id];

       $response = $this->patch('/api/groups/access/menus/update',$data, ['accept'=>'application/json']);
       $response->assertStatus(200)->assertSeeText("Menu updated successfully to {$data['name']}");

   }
}
