<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UserToGroupTest extends TestCase
{
    use refreshDatabase;
     /**
     * tests if the user can add user to a group
     *
     * @test
     */

    public function user_cannot_add_to_group(){
        $new_user = User::factory()->create();
        $group = Group::factory()->create();
        $data = ['group_id'=> $group->id, 'user_id'=>$new_user->id ];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['user:show']
        );

        $response = $this->put('api/users/group/add', $data, ['Accept' =>'Application/json']);

        $response->assertStatus(401);
    }

       /**
     * test if ids are invalid
     *
     * @return void
     *
     * @test
     * @TODO: finish this test
     */
    public function group_and_user_id_to_add_is_invalid_format()
    {
        $new_user = User::factory()->create();
        $group = Group::factory()->create();
        $data = ['group_id'=> 0, 'user_id'=>0 ];

        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['group:add']
        );

        $response = $this->put('api/users/group/add', $data, ['Accept' =>'Application/json']);
        $response->assertStatus(400)->assertJsonStructure( ['group_id','user_id'] );
    }

    /**
     * test if group id to add user to is invalid
     *
     * @return void
     *
     * @test
     */
    public function group_to_add_user_to_not_found()
    {
        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['group:add']
        );

        $new_user = User::factory()->create();
        $group = Group::factory()->create();
        $data = ['group_id'=> 8483487348, 'user_id'=>$new_user->id ];

        $response = $this->put('api/users/group/add', $data, ['Accept' =>'Application/json']);
        $response->assertStatus(404)->assertSeeText("Group with id of {$data['group_id']} not found");
    }

       /**
     * test if user to be added is a valid user id
     *
     * @return void
     *
     * @test
     */
    public function user_to_add_to_group_not_found()
    {
        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['group:add']
        );

        $new_user = User::factory()->create();
        $group = Group::factory()->create();
        $data = ['group_id'=> $group->id, 'user_id'=>42323 ];

        $response = $this->put('api/users/group/add', $data, ['Accept' =>'Application/json']);
        $response->assertStatus(404)->assertSeeText("User with id of {$data['user_id']} not found");
    }

    /**
     * tests for adding user to a group
     *
     * @test
     */
    public function add_user_to_group(){

        $group = Group::factory()->create();
        $new_user = User::factory()->create();
        $data = ['group_id'=> $group->id, 'user_id'=>$new_user->id ];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['group:add']
        );

        $response = $this->put('api/users/group/add', $data, ['Accept' =>'Application/json']);
        $response->assertOk()->assertJsonStructure(
            [
                'id','name','email', 'groups' =>['id', 'name' ]
            ]
        );
    }


    /**
     * test if a user is in a group
     *
     * @test
     */

     public function user_not_in_a_group(){
        $new_user = User::factory()->create();
        $group = Group::factory()->create();

        $data = ['group_id'=> $group->id, 'user_id'=>$new_user->id ];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['group:remove']
        );

        $response = $this->put('api/users/group/remove', $data, ['Accept' =>'Application/json']);
        $response->assertNotFound()->assertSeeText(
            "User with id of $new_user->id not in $group->name group"
        );

     }

      /**
     * tests for adding user to a group
     *
     * @test
     */
    public function remove_user_from_group(){

        $new_user = User::factory()->create();
        $group = Group::factory()->create();

        $group->users()->save(
            $new_user
            );
        $data = ['group_id'=> $group->id, 'user_id'=>$new_user->id ];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['group:remove']
        );

        $response = $this->put('api/users/group/remove', $data, ['Accept' =>'Application/json']);
        $response->assertOk()->assertSeeText(
            "User $new_user->email was successfuly removed from $group->name"
        );

    }


}
