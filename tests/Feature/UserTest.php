<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;
use Illuminate\Support\Str;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * feature test to check if user was disabled
     *
     * @return void
     * @test
     */
    public function disable_user()
    {
        $user = Sanctum::actingAs(
            User::factory()->create(['roles' => 'admin-level-3']), ['*']
        );

        $this->withoutExceptionHandling();

        $response = $this->get("api/users/{$user->id}/disable");
        $response->assertSeeText('account disabled successfully');
        $response->assertStatus(200);
    }

     /**
     * feature test to check if user has permission to disable account
     *
     * @return void
     * @test
     */
    public function user_disable_permission()
    {
        $normal_user = User::factory()->create(['roles' => 'user']);

        $user = Sanctum::actingAs(
            User::factory()->create(['roles' => 'user']), ['user:create']
        );

        $this->withoutExceptionHandling();

        $response = $this->get("api/users/{$normal_user->id}/disable");
        $response->assertSeeText("User ".$user->email ." does not have access");
        $response->assertStatus(401);
    }

    /**
     * displays user based on id
     *
     * @test
     */

    public function user_id_with_tags(){

        $user = Sanctum::actingAs(
             User::factory()->create([
                 'roles' => 'user'
             ]),['user:update','user:create','user:show']
         );

         $id= "<script>{$user->id}</script>";

         $response = $this->get("api/users/$id/show",['Accept' => 'application/json']);
         $response->assertStatus(404);
      }

      /**
     * test if the user entered non integer id
     *
     * @test
     */

     public function user_with_non_Integer_id(){

       $user = Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'user'
            ]),['user:update','user:create','user:show']
        );

        $id= "ddkfjd";

        $response = $this->get("api/users/$id/show",['Accept' => 'application/json']);
        $response->assertJsonStructure([
            'id'
        ]);
        $response->assertStatus(400);
     }

       /**
     * test if the user id is not found in model
     *
     * @test
     */

    public function user_id_not_found(){

        $user = Sanctum::actingAs(
             User::factory()->create([
                 'roles' => 'user'
             ]),['user:update','user:create','user:show']
         );

         $id= 45485745;

         $response = $this->get("api/users/$id/show",['Accept' => 'application/json']);
         $response->assertSeeText("User not found");
         $response->assertStatus(404);
      }



         /**
     * displays user based on id
     *
     * @test
     */


     public function show_user_with_valid_id(){

        $user = Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'user'
            ]),['user:update','user:create','user:show']
        );
        $response = $this->get("api/users/$user->id/show",['Accept' => 'application/json']);
        $response->assertJsonStructure(
            [
                'id','name','email','created_at','updated_at','account_status'
            ]
            );
        $response->assertOk();
     }

     /**
      * tests if a user account has been updated
      * @test
      */

      public function update_user(){
          $data = ['name'=>'james philip'];

          $user = Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'user'
            ]),['user:update','user:create','user:show']
        );

          $response = $this->patch("api/users/update", $data);

          $response->assertOk();
          $response->assertSeeText('User updated successfully');
      }



}
