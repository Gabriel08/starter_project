<?php

namespace Tests\Feature;

use App\Models\Group;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class GroupTest extends TestCase
{
    use RefreshDatabase;
    /**
     * test for invalid name supplied by user e.g name less than the  min value required.
     *
     * @return void
     *
     * @test
     */
    public function create_group_with_invalid_name_format()
    {
        $data = ['name'=>'A#'];

        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['*']
        );

        $response = $this->post('/api/groups/create',$data, ['accept'=>'application/json']);
        $response->assertJsonStructure([
            'name'
        ]);
        $response->assertStatus(400);
    }

    /**
     * test if user has the right to create a new group
     *
     * @return void
     *
     * @test
     */
    public function user_cannot_create_group()
    {
        $data = ['name'=>'Accounting'];

        $user = Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'user',
            ]),['user:create']
        );

        $response = $this->post('/api/groups/create',$data, ['accept'=>'application/json']);
        $response->assertStatus(401);
    }

     /**
     * test for valid group name
     *
     * @return void
     *
     * @test
     */
    public function create_group_with_valid_name_format()
    {
        $data = ['name'=>'Adjfkdjfdkj'];

        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['*']
        );

        $response = $this->post('/api/groups/create',$data, ['accept'=>'application/json']);
        $response->assertJsonStructure([
            'name', 'updated_at', 'created_at','id'
        ]);
        $response->assertStatus(201);
    }

        /**
     * test for invalid name supplied by user e.g name less than the  min value required.
     *
     * @return void
     *
     * @test
     */
    public function update_group_with_invalid_name_format()
    {
        $data = ['name'=>'A#'];

        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['*']
        );

        $response = $this->patch('/api/groups/update',$data, ['accept'=>'application/json']);
        $response->assertStatus(400);
        $response->assertJsonStructure([
            'name'
        ]);

    }

    /**
     * test if user has the right to create a new group
     *
     * @return void
     *
     * @test
     */
    public function user_cannot_update_group()
    {
        $data = ['name'=>'Accounting'];

        $user = Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'user',
            ]),['user:update']
        );

        $response = $this->patch('/api/groups/update',$data, ['accept'=>'application/json']);
        $response->assertStatus(401);
    }

     /**
     * test for valid group name
     *
     * @return void
     *
     * @test
     */
    public function update_group_with_valid_name_format()
    {

        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['*']
        );

        $group = Group::factory()->create();

        $data = ['name'=>'international','id' =>$group->id];

        $response = $this->patch('/api/groups/update',$data, ['accept'=>'application/json']);
        $response->assertStatus(200);
        $response->assertSeeText("Group updated successfully to {$data['name']}");

    }

     /**
     * test if user can delete group
     *
     * @return void
     *
     * @test
     */
    public function user_can_delete_group()
    {

        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['group:delete']
        );

        $group = Group::factory()->create();

        $response = $this->delete("/api/groups/{$group->id}/delete");

        $response->assertStatus(200);
        $response->assertSeeText("{$group->name} Group was deleted successfully");

    }

     /**
     * test if a user cannot delete group
     *
     * @return void
     *
     * @test
     */
    public function user_cannot_delete_group()
    {
        $user = Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'user',
            ]),['user:delete']
        );

        $group = Group::factory()->create();

        $response = $this->delete("/api/groups/{$group->id}/delete");

        $response->assertStatus(401);
        $response->assertSeeText("User {$user->email} does not have access");
    }

       /**
     * test if id is invalid
     *
     * @return void
     *
     * @test
     */
    public function group_id_not_found()
    {
        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['group:delete']
        );

        $group_id = 8483487348;

        $response = $this->delete("/api/groups/{$group_id}/delete");
        $response->assertStatus(404);
        $response->assertSeeText("Group with id of {$group_id} not found");
    }

       /**
     * test if id is invalid
     *
     * @return void
     *
     * @test
     */
    public function group_id_is_invalid()
    {
        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['*']
        );

        $group_id = 0;

        $response = $this->delete("/api/groups/{$group_id}/delete");
        $response->assertStatus(400);
        $response->assertJsonStructure(
            ['id']
        );
    }

    /**
     * tests if the user can show a group
     *
     * @test
     */

     public function user_cannot_show_group(){
        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'user',
            ]),['user:show']
        );

        $response = $this->get('/api/groups/$id/show', ['Accept'=>'application/json']);

        $response->assertStatus(401);
     }

        /**
     * test if id is invalid
     *
     * @return void
     *
     * @test
     */
    public function group_id_to_show_is_invalid()
    {
        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['*']
        );

        $group_id = 0;

        $response = $this->get("/api/groups/{$group_id}/show");
        $response->assertStatus(400);
        $response->assertJsonStructure(
            ['id']
        );
    }

        /**
     * test if group id to show is invalid
     *
     * @return void
     *
     * @test
     */
    public function group_to_show_not_found()
    {
        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['group:show']
        );

        $group_id = 8483487348;

        $response = $this->get("/api/groups/{$group_id}/show");
        $response->assertStatus(404);
        $response->assertSeeText("Group with id of {$group_id} not found");
    }

        /**
     * test user show group
     *
     * @return void
     *
     * @test
     */
    public function user_show_group()
    {
        Sanctum::actingAs(
            User::factory()->create([
                'roles' => 'admin-level-3',
            ]),['group:show']
        );


        $group = Group::factory()->create();

        $response = $this->get("/api/groups/{$group->id}/show");
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'name','created_at','updated_at','id'
        ]);
    }
}
