<?php

namespace Tests\Feature;

use App\Models\AccessModules;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ModuleDeleteTest extends TestCase
{
    use RefreshDatabase;

    /**
    * test for invalid id supplied by user e.g name less than the  min value required.
    *
    * @return void
    *
    * @test
    */
   public function remove_module_with_invalid_id_format()
   {
       $data = ['id'=>0];

       Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'admin-level-3',
           ]),['module:delete']
       );

       $response = $this->delete('/api/groups/access/modules/remove',$data, ['accept'=>'application/json']);
       $response->assertStatus(400)->assertJsonStructure(['id']);

   }

   /**
    * test if user has the right to delete a module
    *
    * @return void
    *
    * @test
    */
   public function user_access_cannot_delete_module()
   {
       $module = AccessModules::factory()->create();
        $data = ['id'=>$module->id];

     $user = Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'user',
           ]),['user:create']
       );

       $response = $this->delete('/api/groups/access/modules/remove',$data, ['accept'=>'application/json']);
       $response->assertStatus(401)->assertSeeText("User $user->email does not have access");
   }

   /**
    * test if module is found
    *
    * @return void
    *
    * @test
    */
   public function module_not_found()
   {

        $data = ['id'=>3];

       Sanctum::actingAs(
           User::factory()->make([
               'roles' => 'admin-level-3',
           ]),['module:delete']
       );

       $response = $this->delete('/api/groups/access/modules/remove',$data, ['accept'=>'application/json']);
       $response->assertStatus(404)->assertSeeText("Module with id of {$data['id']} not found");
   }

    /**
    * test for valid module name
    *
    * @return void
    *
    * @test
    */
   public function delete_module_with_valid_credentials()
   {

        $module = AccessModules::factory()->create();
        $data = ['id'=>$module->id];

        $user = Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['module:delete']
        );

        $response = $this->delete('/api/groups/access/modules/remove',$data, ['accept'=>'application/json']);
       $response->assertStatus(200)->assertSeeText("$module->name Module was deleted successfully");

   }
}
