<?php

namespace Tests\Feature;

use App\Models\AccessModules;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ModuleUpdateTest extends TestCase
{
    use RefreshDatabase;
     /**
     * test for invalid name supplied by user e.g name less than the  min value required.
     *
     * @return void
     *
     * @test
     */
    public function update_module_with_invalid_name_format()
    {
        $data = ['name'=>'Asd#','id'=>1];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['module:update']
        );

        $response = $this->patch('/api/groups/access/modules/update',$data, ['accept'=>'application/json']);
        $response->assertStatus(400)->assertJsonStructure([
            'name'
        ]);

    }

     /**
     * test for invalid id supplied by user e.g name less than the  min value required.
     *
     * @return void
     *
     * @test
     */
    public function update_module_with_invalid_id_format()
    {
        $data = ['name'=>'Asd Hello','id'=>0];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['module:update']
        );

        $response = $this->patch('/api/groups/access/modules/update',$data, ['accept'=>'application/json']);
        $response->assertStatus(400)->assertJsonStructure(['id']);

    }

    /**
     * test if user has the right to update a module
     *
     * @return void
     *
     * @test
     */
    public function user_access_cannot_update_module()
    {
        $module = AccessModules::factory()->create();
         $data = ['name'=>'Asd Hello','id'=>$module->id];

       $user =  Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'user',
            ]),['user:create']
        );

        $response = $this->patch('/api/groups/access/modules/update',$data, ['accept'=>'application/json']);

        $response->assertStatus(401)->assertSeeText("User $user->email does not have access");
    }

    /**
     * test if user has the right to update a module
     *
     * @return void
     *
     * @test
     */
    public function module_not_found()
    {

         $data = ['name'=>'Asd Hello','id'=>3];

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['module:update']
        );

        $response = $this->patch('/api/groups/access/modules/update',$data, ['accept'=>'application/json']);
        $response->assertStatus(404)->assertSeeText("Module not found");
    }

     /**
     * test for valid module name
     *
     * @return void
     *
     * @test
     */
    public function update_module_with_valid_credentials()
    {

        Sanctum::actingAs(
            User::factory()->make([
                'roles' => 'admin-level-3',
            ]),['module:update']
        );

        $module = AccessModules::factory()->create();
        $data = ['name'=>'international','id' =>$module->id];

        $response = $this->patch('/api/groups/access/modules/update',$data, ['accept'=>'application/json']);
        $response->assertStatus(200)->assertSeeText("Module updated successfully to {$data['name']}");

    }

}
