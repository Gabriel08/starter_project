<?php

use App\Http\Controllers\Api\AccessMenuController;
use App\Http\Controllers\Api\AccessModulesController;
use App\Http\Controllers\Api\AccessSectionsController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\GroupController;
use App\Http\Controllers\Api\RegistrationController;
use App\Http\Controllers\Api\UserController;
use App\Models\AccessMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/users/create', [RegistrationController::class,'store'])->name('register');
Route::post('/login', [AuthController::class,'login'])->name('login');

Route::group(['middleware'=>['auth:sanctum'] ], function(){
    Route::get('/logout', [AuthController::class,'logout'])->name('logout');
    Route::patch('/password/reset',[AuthController::class,'password_reset'])->name('password_reset');

    Route::group(['prefix' =>'users'], function(){
        Route::get('/', [UserController::class, 'index'])->name('all_users');
        Route::get('/{id}/disable', [UserController::class, 'disable'])->name('disbale_user');
        Route::get('/{id}/show', [UserController::class, 'show'])->name('show_user');
        Route::patch('/update',[UserController::class,'update'])->name('update_user');

        Route::group(['prefix' =>'group'], function(){
            Route::put('/add', [UserController::class, 'add_user_to_group'])->name('add_user_to_group');
            Route::put('/remove', [UserController::class, 'remove_user_from_group'])->name('remove_user_from_group');
        });

    });

    Route::group(['prefix' =>'groups'], function(){
        Route::get('/', [GroupController::class, 'index'])->name('all_groups');
        Route::post('/create', [GroupController::class, 'store'])->name('create_group');
        Route::get('{id}/show', [GroupController::class, 'show'])->name('show_group');
        Route::patch('/update',[GroupController::class,'update'])->name('update_group');
        Route::delete('{id}/delete',[GroupController::class,'destroy'])->name('delete_group');

        Route::group(['prefix' =>'access'], function(){
            Route::group(['prefix' =>'modules'], function(){
                Route::get('/', [AccessModulesController::class, 'index'])->name('module_all');
                Route::post('/create', [AccessModulesController::class, 'store'])->name('module_create');
                Route::patch('/update', [AccessModulesController::class, 'update'])->name('module_update');
                Route::delete('/remove', [AccessModulesController::class, 'destroy'])->name('module_remove');
            });

            Route::group(['prefix' =>'sections'], function(){
                Route::get('/', [AccessSectionsController::class, 'index'])->name('section_all');
                Route::post('/create', [AccessSectionsController::class, 'store'])->name('section_create');
                Route::patch('/update', [AccessSectionsController::class, 'update'])->name('section_update');
                Route::delete('/remove', [AccessSectionsController::class, 'destroy'])->name('section_remove');
            });

            Route::group(['prefix' =>'menus'], function(){
                Route::get('/', [AccessMenuController::class, 'index'])->name('menu_all');
                Route::post('/create', [AccessMenuController::class, 'store'])->name('menu_create');
                Route::patch('/update', [AccessMenuController::class, 'update'])->name('menu_update');
                Route::delete('/remove', [AccessMenuController::class, 'destroy'])->name('menu_remove');
            });
        });
    });

});

