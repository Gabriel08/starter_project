<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Abilities;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\Password;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{

    /**
     * Retrieved user details and allows access if credentials are correct
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){

        $validator = validator($request->only('email','password'), [
            'email' => ['required', 'email'],
            'password' => ['required', Password::min(5)->mixedCase()->uncompromised()]
        ]);


        if($validator->fails()){
            Log::channel('abuse')->debug("User {$request->email} entered {$validator->errors() }");
            return response()->json($validator->errors(), 400);

        }

        $user = User::where('email', $request->email)->first();

        if(!$user){
            Log::channel('invt_exceptions')->debug("User {$request->email} was not found ");
            return response()->json("Email not found", 404);
        }

         $valid_password = Hash::check($request->password, $user->password);

         if(!$valid_password){
            return response()->json("Invalid user credentials",404);
         }

         if(!$user->account_status){
            return response()->json("Account disabled");
         }

         if($user->roles =='user'){
            $abilities = Abilities::where('min_level',0)->get()->toArray();
            $token = $user->createToken($user->email,$abilities)->plainTextToken;
         }
         elseif($user->roles =='admin-level-1'){
            $abilities = Abilities::where('min_level','<=',1)->get()->toArray();
            $token = $user->createToken('admin1', $abilities)->plainTextToken;
         }elseif($user->roles =='admin-level-2'){
            $abilities = Abilities::where('min_level','<=',2)->get()->toArray();
            $token = $user->createToken('admin2', $abilities)->plainTextToken;
         }elseif($user->roles =='admin-level-3'){
            $token = $user->createToken('admin3',['*'])->plainTextToken;
         }
         return response()->json(['token'=>$token,'user'=>$user]);
    }

    public function logout(){
        // this only works becuase the of the auth::sanctum middleware

       auth()->user()->tokens()->delete();
       return response('user logged out');

    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function password_reset(Request $request){

        $validator = validator($request->only('current_password','new_password'), [
            'current_password' => ['required', Password::min(5)->mixedCase()->uncompromised()],
            'new_password' => ['required', Password::min(5)->mixedCase()->uncompromised()]
        ]);

        if($validator->fails()){
            Log::channel('abuse')->debug("User ".auth()->user()->email." entered {$validator->errors() }");
            return response()->json($validator->errors(), 400);
        }

        if($request->current_password == $request->new_password ){
            return response()->json("Current and new password cannot be the same", 400);
        }

            if( !Hash::check($request->current_password, auth()->user()->password) ){
                return response()->json("incorrect password", 500);
            }

        $user = User::where('id', auth()->user()->id)->first();
        $user->password = Hash::make($request->new_password);
        $user->save();

        return response()->json("password reset successful");

     }
}
