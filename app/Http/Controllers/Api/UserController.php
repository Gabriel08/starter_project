<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\User;
use App\Rules\SanitizeString;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

use function PHPUnit\Framework\isNan;

class UserController extends Controller
{

    /**
         * display all users
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function index(){
            if(!auth()->user()->tokenCan('admin:show:users')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised operation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }
            $user = User::get();

            if(count($user) == 0){
                return response()->json("No User found", 200);
            }
            return response()->json($user, 200);
        }

    /**
     * disables a user account, can only be do my admin
     *
     * @param int id
     * @return \Illuminate\Http\Response
     *
     */
     public function disable($id){

        $validator =   validator(['id' =>$id], [
            'id' => ['integer','min:1','required']
          ]);

          if  ($validator->fails() ){
            Log::channel('abuse')->alert( auth()->user()->email." entered '{$id}' as the parameter of user id for 'show_user' endpoint");
              return response()->json($validator->errors(), 400);
          }



            $user = User::where('id',$id)->first();

            if(!$user){
                return response()->json("user not found", 404);
            }

         if(auth()->user()->tokenCan('user:disable') ){
            $user->account_status = false;
            $user->save();

            return response()->json("account disabled successfully");
         }

         Log::channel('abuse')->info('attempting to perform an unauthorised operation');
         return response()->json("User ".auth()->user()->email ." does not have access", 401);
     }

     /**
      * displays user details
      * @param int id
      * @return \Illuminate\Http\Response
      */

      public function show($id){

        $validator =   validator(['id' =>$id], [
            'id' => ['integer','min:1','required']
          ]);

          if ($validator->fails() ){
            Log::channel('abuse')->alert( auth()->user()->email." entered '{$id}' as the parameter of user id for 'show_user' endpoint");
              return response()->json($validator->errors(), 400);
          }
            $user = User::where('id', $id)->first();

            if(!$user){
                return response()->json("User not found", 404);
            }

            return response()->json($user);

      }

      /**
         * Updates users account name
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
      public function update(Request $request){
        $validator = validator($request->only('name'), [
            'name' => ['required','string','min:5', new SanitizeString]
        ]);


        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        $id = auth()->user()->id;

        $user = User::where('id', $id)->first();


        if(!$user){
            return response()->json("User not found", 404);
        }

        $user->name = $request->name;
        $user->save();

        return response()->json("User updated successfully");

      }


       /**
         * adds user to a group
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
      public function add_user_to_group(Request $request){

        if(!auth()->user()->tokenCan('group:add')){
            Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised operation of adding a user to group");
            return response()->json("User ".auth()->user()->email." does not have access", 401);
        }

        $validator =   validator($request->only('group_id','user_id'), [
            'group_id' => ['integer','min:1','required'],
            'user_id' => ['integer','min:1','required'],
          ]);

          if ($validator->fails() ){
            Log::channel('abuse')->alert( auth()->user()->email." entered '{$request->group_id}' and '{$request->user_id}' as the parameter of group_id and user id for 'group/add' endpoint");
              return response()->json($validator->errors(), 400);
          }

          $group = Group::where('id',$request->group_id)->first();
          if(!$group){
            return response()->json("Group with id of {$request->group_id} not found", 404);
          }

          $user = User::where('id',$request->user_id)->first();
          if(!$user){
            return response()->json("User with id of {$request->user_id} not found", 404);
          }

          $group->users()->save(
            $user
            );

          $user = $user->select(['id','name','email','group_id'])->with(['groups' =>function($query){
                return $query->select(['id','name']);
          }])->first();
          return response()->json($user);

      }

      /**
       * This function is to remove a user from a group
       *
       * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
       */
      public function remove_user_from_group(Request $request){

        if(!auth()->user()->tokenCan('group:remove')){
            Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised operation of removing a user to group");
            return response()->json("User ".auth()->user()->email." does not have access", 401);
        }

        $validator =   validator($request->only('group_id','user_id'), [
            'group_id' => ['integer','min:1','required'],
            'user_id' => ['integer','min:1','required'],
          ]);

          if ($validator->fails() ){
            Log::channel('abuse')->alert( auth()->user()->email." entered '{$request->group_id}' and '{$request->user_id}' as the parameter of group_id and user id for 'group/add' endpoint");
              return response()->json($validator->errors(), 400);
          }

          $group = Group::where('id',$request->group_id)->first();
          if(!$group){
            return response()->json("Group with id of {$request->group_id} not found", 404);
          }

          $user = User::where('id',$request->user_id)->first();
          if(!$user){
            return response()->json("User with id of {$request->user_id} not found", 404);
          }

          if(!$user->group_id){
            return response()->json("User with id of {$request->user_id} not in $group->name group", 404);
          }

            $removed = $group->users()->update(
            ['group_id' => null]
            );

            if($removed){
                return response()->json("User $user->email was successfuly removed from $group->name");
            }

            Log::channel('abuse')->info( auth()->user()->email." attempted to remove $user->email from $group->name ");
            return response()->json("Unable to remove $user->email from $group->name", 500);


        }
}
