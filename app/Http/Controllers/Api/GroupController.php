<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Rules\SanitizeString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GroupController extends Controller
{

        /**
         * display all groups
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function index(){
            if(!auth()->user()->tokenCan('group:show')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised operation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }
            $group = Group::get();

            if(count($group) == 0){
                return response()->json("No Group found", 200);
            }
            return response()->json($group, 200);
        }
        /**
         * creates groups
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
    public function store(Request $request){

        if(!auth()->user()->tokenCan('group:create')){
            Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised operation");
            return response()->json("User ".auth()->user()->email." does not have access", 401);
        }

        $validator = validator($request->only('name'), [
            'name' => ['required','string','min:2', new SanitizeString]
        ]);

        if($validator->fails()){
            Log::channel('abuse')->info('attempting to perform an invalid input group name');
            return response()->json($validator->errors(), 400);
        }

        if(Group::where('name',$request->name)->first()){
            return response()->json("Group already exists", 422);
        }
        $group = Group::create([
            'name' => $request->name
        ]);

        return response()->json($group, 201);
    }

        /**
         * Updates group account name
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request){

            if(!auth()->user()->tokenCan('group:create')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised operation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator = validator($request->only('name'), [
                'name' => ['required','string','min:2', new SanitizeString]
            ]);


            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }


            $group = Group::where('id', $request->id)->first();


            if(!$group){
                return response()->json("group not found", 404);
            }
            $group->name = $request->name;
            $group->save();

            return response()->json("Group updated successfully to {$request->name}");

        }

          /**
         * Deletes a group
         *
         * @param  int id
         * @return \Illuminate\Http\Response
         */

        public function destroy($id){

            if(!auth()->user()->tokenCan('group:delete')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised deleteoperation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator =   validator(['id' =>$id], [
                'id' => ['integer','min:1','required']
              ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }

            $group = Group::where('id',$id)->first();

            if(!$group){
                return response()->json("Group with id of {$id} not found", 404);
            }

            if(!$group->delete()){
                return response()->json("Unable to delete Group", 500);
            }
           return response()->json("{$group->name} Group was deleted successfully");

        }


          /**
         * Shows a group
         *
         * @param  int id
         * @return \Illuminate\Http\Response
         */

        public function show($id){
            if(!auth()->user()->tokenCan('group:show')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised operation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator =   validator(['id' =>$id], [
                'id' => ['integer','min:1','required']
              ]);

              if  ($validator->fails() ){
                Log::channel('abuse')->alert( auth()->user()->email." entered '{$id}' as the parameter of user id for 'show_user' endpoint");
                  return response()->json($validator->errors(), 400);
              }

              $group = Group::where('id', $id)->first();


            if(!$group){
                return response()->json("Group with id of {$id} not found", 404);
            }

            return response()->json($group);
        }

}
