<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AccessMenu;
use App\Models\AccessSections;
use App\Rules\SanitizeString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AccessMenuController extends Controller
{
     /**
         * display all menus
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function index(){
            if(!auth()->user()->tokenCan('menu:show')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised operation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }
            $menu = AccessMenu::get();

            if(count($menu) == 0){
                return response()->json("No Menu found", 200);
            }
            return response()->json($menu, 200);
        }

        /**
         * creates new menus
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request){

            if(!auth()->user()->tokenCan('menu:create')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised creating menu");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator = validator($request->only('name','section_id'), [
                'name' => ['required','string','min:2', new SanitizeString],
                'section_id' => ['integer','min:1','required'],
            ]);

            if($validator->fails()){
                Log::channel('abuse')->info('attempting to perform an invalid input menu name');
                return response()->json($validator->errors(), 400);
            }

            $access_section = AccessSections::where('id', $request->section_id)->first();

            if(!$access_section){
                return response()->json("Section not found", 404);
            }

            if(AccessMenu::where('name',$request->name)->first()){
                return response()->json("Menu already exists", 422);
            }

            $new_menu = AccessMenu::create(['name' => $request->name]);
            $access_menu = $access_section->access_menus()->save($new_menu);

            return response()->json($access_menu, 201);
        }

        /**
         * Updates menu name
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request){

            if(!auth()->user()->tokenCan('menu:update')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to update a menu without right permissions");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator = validator($request->only('name','id'), [
                'name' => ['required','string','min:2', new SanitizeString],
                'id' => ['integer','min:1','required'],
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }

            $access_menu = AccessMenu::where('id', $request->id)->first();

            if(!$access_menu){
                return response()->json("Menu not found", 404);
            }
            $access_menu->name = $request->name;
            $access_menu->save();

            return response()->json("Menu updated successfully to {$request->name}");
        }

          /**
         * Deletes a menu
         *
         * @param  int id
         * @return \Illuminate\Http\Response
         */

         public function destroy(Request $request){

            if(!auth()->user()->tokenCan('menu:delete')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised delete operation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator =   validator($request->only('id'), [
                'id' => ['integer','min:1','required']
              ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }

            $access_menu = AccessMenu::where('id',$request->id)->first();

            if(!$access_menu){
                return response()->json("Menu with id of {$request->id} not found", 404);
            }

            if(!$access_menu->delete()){
                return response()->json("Unable to delete Menu", 500);
            }
           return response()->json("{$access_menu->name} Menu was deleted successfully");

        }
}
