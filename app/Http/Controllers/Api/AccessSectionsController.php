<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AccessModules;
use App\Models\AccessSections;
use App\Rules\SanitizeString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AccessSectionsController extends Controller
{

        /**
         * display all sections
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function index(){
            if(!auth()->user()->tokenCan('section:show')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised operation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }
            $sections = AccessSections::get();

            if(count($sections) == 0){
                return response()->json("No Section found", 200);
            }
            return response()->json($sections, 200);
        }

        /**
         * creates new sections
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request){

            if(!auth()->user()->tokenCan('section:create')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised creating section");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator = validator($request->only('name','module_id'), [
                'name' => ['required','string','min:2', new SanitizeString],
                'module_id' => ['integer','min:1','required'],
            ]);

            if($validator->fails()){
                Log::channel('abuse')->info('attempting to perform an invalid input section name');
                return response()->json($validator->errors(), 400);
            }

            $access_module = AccessModules::where('id', $request->module_id)->first();

            if(!$access_module){
                return response()->json("Module not found", 404);
            }

            if(AccessSections::where('name',$request->name)->first()){
                return response()->json("Section already exists", 422);
            }

            $new_section = AccessSections::create(['name' => $request->name]);
            $access_section = $access_module->access_sections()->save($new_section);

            return response()->json($access_section, 201);
        }

        /**
         * Updates section name
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request){

            if(!auth()->user()->tokenCan('section:update')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to update a section without right permissions");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator = validator($request->only('name','id'), [
                'name' => ['required','string','min:2', new SanitizeString],
                'id' => ['integer','min:1','required'],
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }

            $access_section = AccessSections::where('id', $request->id)->first();

            if(!$access_section){
                return response()->json("Section not found", 404);
            }
            $access_section->name = $request->name;
            $access_section->save();

            return response()->json("Section updated successfully to {$request->name}");
        }

          /**
         * Deletes a section
         *
         * @param  int id
         * @return \Illuminate\Http\Response
         */

         public function destroy(Request $request){

            if(!auth()->user()->tokenCan('section:delete')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised delete operation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator =   validator($request->only('id'), [
                'id' => ['integer','min:1','required']
              ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }


            $access_section = AccessSections::where('id',$request->id)->first();
            if(!$access_section){
                return response()->json("Section with id of {$request->id} not found", 404);
            }

            if(!$access_section->delete()){
                return response()->json("Unable to delete Section", 500);
            }
           return response()->json("{$access_section->name} Section was deleted successfully");

        }
}
