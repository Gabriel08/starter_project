<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Rules\SanitizeString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class RegistrationController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *  new SanitizeString make sure only word characters are allowed
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 400 is bad request
     */
    public function store(Request $request)
    {

        $validator = validator($request->only('name','email','password'), [
            'name' => ['required','string','min:5', new SanitizeString],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', Password::min(5)->mixedCase()->uncompromised()]
        ]);


        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }

        if(User::where('email',$request->email)->first()){
            return response()->json("User already exists", 422);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' =>Hash::make($request->password)
        ]);


        if($user){
            return response()->json(['user' => $user], 201);
        }
    }



}
