<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AccessModules;
use App\Models\Group;
use App\Rules\SanitizeString;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AccessModulesController extends Controller
{
     /**
         * display all modules
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function index(){
            if(!auth()->user()->tokenCan('module:show')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised operation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }
            $modules = AccessModules::get();

            if(count($modules) == 0){
                return response()->json("No Module found", 200);
            }
            return response()->json($modules, 200);
        }

        /**
         * creates new modules
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request){

            if(!auth()->user()->tokenCan('module:create')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised creating module");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator = validator($request->only('name','group_id'), [
                'name' => ['required','string','min:2', new SanitizeString],
                'group_id' => ['integer','min:1','required'],
            ]);

            if($validator->fails()){
                Log::channel('abuse')->info('attempting to perform an invalid input module name');
                return response()->json($validator->errors(), 400);
            }

            $group = Group::where('id', $request->group_id)->first();

            if(!$group){
                return response()->json("group not found", 404);
            }

            if(AccessModules::where('name',$request->name)->first()){
                return response()->json("Module already exists", 422);
            }

            $new_module = AccessModules::create(['name' => $request->name]);
            $access_module = $group->accessmodules()->save($new_module);

            return response()->json($access_module, 201);
        }

        /**
         * Updates module name
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request){

            if(!auth()->user()->tokenCan('module:update')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to update a module wothout right permissions");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator = validator($request->only('name','id'), [
                'name' => ['required','string','min:2', new SanitizeString],
                'id' => ['integer','min:1','required'],
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }

            $access_modules = AccessModules::where('id', $request->id)->first();


            if(!$access_modules){
                return response()->json("Module not found", 404);
            }
            $access_modules->name = $request->name;
            $access_modules->save();

            return response()->json("Module updated successfully to {$request->name}");
        }

          /**
         * Deletes a module
         *
         * @param  int id
         * @return \Illuminate\Http\Response
         */

         public function destroy(Request $request){

            if(!auth()->user()->tokenCan('module:delete')){
                Log::channel('abuse')->info( auth()->user()->email." attempting to perform an unauthorised delete operation");
                return response()->json("User ".auth()->user()->email." does not have access", 401);
            }

            $validator =   validator($request->only('id'), [
                'id' => ['integer','min:1','required']
              ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }

            $access_module = AccessModules::where('id',$request->id)->first();

            if(!$access_module){
                return response()->json("Module with id of {$request->id} not found", 404);
            }

            if(!$access_module->delete()){
                return response()->json("Unable to delete Module", 500);
            }
           return response()->json("{$access_module->name} Module was deleted successfully");

        }
}
