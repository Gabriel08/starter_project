<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    /**
     * The attributes that can be mass saved into the database
     *
     * @var array
     */
    protected $fillable=['name'];

     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
    ];

    protected $table='groups_table';

    public function users(){
        return $this->hasMany(User::class,'group_id','id');
    }

    public function accessmodules(){
        return $this->hasMany(AccessModules::class,'group_id','id');
    }
}
