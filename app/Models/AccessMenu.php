<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessMenu extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function access_sections(){
        return $this->belongsTo(AccessSections::class,'section_id','id');
    }
}
