<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessModules extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function groups(){
        return $this->belongsTo(Group::class,'group_id','id');
    }

    public function access_sections(){
        return $this->hasMany(AccessSections::class,'module_id','id');
    }
}
