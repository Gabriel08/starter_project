<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessSections extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function access_modules(){
        return $this->belongsTo(AccessModules::class,'module_id','id');
    }
    public function access_menus(){
        return $this->hasMany(AccessMenu::class,'section_id','id');
    }
}
