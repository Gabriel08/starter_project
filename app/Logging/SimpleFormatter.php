<?php

namespace App\Logging;

use Monolog\Formatter\LineFormatter;

class SimpleFormatter{

    /**
     * this is reposible for formatting the abuse channel of the error log
     */
    public function __invoke($logger){
        foreach($logger->getHandlers() as $handler){
            $handler->setFormatter(
                new LineFormatter('[%datetime%]: %message% %context%')
            );
        }
    }
}
