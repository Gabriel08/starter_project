<?php
namespace App\Services;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class InvtValidation{

    public function validate($input){
        $validator = Validator::make($input, [
            'name' => ['required','string','min:5'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', Password::min(5)->mixedCase()->uncompromised()]
        ]);


        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
    }
}
