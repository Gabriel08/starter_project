
## INVT

INVT is a testing api for user and groups.....


## Project Structure

### App users:
 There are two major types of users:
 - user ( Every day, non-staff or technical users whose only need for the endpoint is to create, update or reset passwords for their accounts)
 - admin ( technical users of the project who oversee the operations of the normal users). The admin is further 
    divided into 3 categories:
    - admin-level-1 (has all user permissions and ability to create, update and show group only)
    - admin-level-2 (has all user permissions, admin-level-1 permissions, and delete a group, create and update modules, sections and menu )
    - admin-level-3 ( has * permissions)

### Project retrictions
- only an admin user can disable or  show an account, add or remove a user from a group
- only admin-level-3 can delete groups, modules, sections,and menu

### module relationships
GROUPS AND USER: This is a one to many relationship where a group can have many users but a user can only belong to a group
GROUPS AND MODULES: One to Many relationship
MODULE AND SECTIONS: one to many relationship
SECTIONS AND MENU: one to many relationship

- the structure can be viewed as: group > modules > sections > menu, group > user. Where chilc chilc only has a direct relationship with its parent



A module is automatically added to a group at the point of creation using the group id supplied by the user. Same thing applies to sections and menu


## Installation/ Setup
- Clone this repository
- run composer install
- php artisan migrate
- db:seed ( to insert the admins into the database)
- run php artisan serve
